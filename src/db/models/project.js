const {Model} = require('objection');

class Todos extends Model {
    static get tableName(){
        return 'todos';
    }

    static get relationMappings(){
        const Task = require('./task');
        return {
            task: {
                relation : Model.HasManyRelation,
                modelClass: Task,
                join: {
                    from : "todos.id",
                    to : "tasks.todoId",
                }
            }
            
        }
    }
}

module.exports = Todos;