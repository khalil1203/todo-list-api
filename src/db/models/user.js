const {Model} = require('objection');

class User extends Model {
    static get tableName(){
        return 'users';
    }

    static get relationMappings(){
        const Todos = require('./project');
        return {
            project: {
                relation : Model.HasManyRelation,
                modelClass: Todos,
                join: {
                    from : "users.id",
                    to : "todos.userId"
                }
            }
            
        }
    }
}

module.exports = User;