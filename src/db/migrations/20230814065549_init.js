/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema
    .createTable("users", function (table) {
      table.increments("id").unique();
      table.string("username",255).unique().notNullable();
      table.string("first_name", 255).notNullable();
      table.string("last_name", 255).notNullable();
      table.string("password", 255).notNullable();
      table.string("email", 255).notNullable().unique();
    })
    .createTable("todos", function (table) {
      table.increments("id");
      table.string("title", 255).notNullable();
      table.string("description", 255).notNullable();
      table.boolean("todoStatus");
      table.integer("userId").references("id").inTable("users");
    })
    .createTable("tasks", function (table) {
      table.increments("id");
      table.string("first_name", 255).notNullable();
      table.boolean("status");
      table.integer("userId").references("id").inTable("users");
      table.integer("todoId").references("id").inTable("todos");
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.down = function (knex) {
  return knex.schema
  .dropTableIfExists("tasks")
  .dropTableIfExists("todos")
    .dropTableIfExists("users");
};
