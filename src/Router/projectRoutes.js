const express = require("express");
const router = express.Router();
const projectController = require("../Controller/project");
const { authentication } = require("../service/auth");

router.post("/:username",authentication, projectController.createProject);
router.get("/:id", authentication,projectController.getProject);
router.put("/:id", authentication,projectController.updateProject);
router.delete("/:id", authentication,projectController.deleteProject);

module.exports = router;
