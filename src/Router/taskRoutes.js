const express = require('express');
const router = express.Router();

const taskController = require('../Controller/task');
const { authentication } = require('../service/auth');

router.post("/",authentication ,taskController.createTask);
router.get("/:id",authentication ,taskController.getTask);
router.put("/:id",authentication ,taskController.updateTask);
router.delete("/:id",authentication ,taskController.deleteTask);

module.exports = router;