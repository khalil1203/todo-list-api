const express = require("express");
const router = express.Router();
const userController = require("../Controller/users");
const { authentication } = require("../service/auth");


router.post("/",userController.createUser);
router.get("/:username",authentication,userController.getUserProfile);
router.post("/login",userController.login);



module.exports = router;
