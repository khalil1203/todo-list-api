const jwt = require("jsonwebtoken");
const config = require("../../config");

 const tokenGenerate = (user) =>{
    
  return jwt.sign(
    { id: user.id, username: user.username },
    config.SECRETKEY
  );
}

 const authentication =async (req, res, next) => {
  const header = req.header('Authorization');
    if(!header){
       return  res.json({data:"Not a valid user!!!"});
    }
    const token=jwt.verify(header,config.SECRETKEY);
    req.token=token;
    next();


};

module.exports={
    tokenGenerate,
    authentication
}
