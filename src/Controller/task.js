const Task = require("../db/models/task");
const Todos = require("../db/models/project");

const createTask = async (req, res) => {
  const { first_name,todoId, status } = req.body;
  const userId = req.token.id;
  const project = await Todos.query().findOne("id", todoId);
  console.log(project);
  try {
    if (project.userId == req.token.id && userId == req.token.id) {
      const task = await Task.query().insert({
        first_name: first_name,
        status: status,
        todoId: todoId,
        userId: userId,
      });
      res.status(200).json({ task: task });
    } else {
      res.status(404).json({ message: "Cannot create Task!!!" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "error creating task!!!" });
  }
};

async function updateProject(id) {
  console.log(id);
  const projectArr = await Task.query().where("todoId", id);
  console.log(projectArr);

  let completedArr = projectArr.filter((entry) => {
    return entry.status;
  });
  if (projectArr.length == completedArr.length) {
    try {
      const project = await Todos.query()
        .where("id", id)
        .update({ todoStatus: true })
        .returning("*");
    } catch (error) {
      console.log(error);
    }
  } else {
    try {
      const project = await Todos.query()
        .where("id", id)
        .update({ todoStatus: false })
        .returning("*");
    } catch (error) {
      console.log(error);
    }
  }
}

const updateTask = async (req, res) => {
  const id = req.params.id;
  const { first_name, status } = req.body;
  const task = (task = await Task.query().findById(id));

  try {
    if (req.token.id == task.userId) {
      task = await Task.query()
        .findById(id)
        .update({ first_name, status })
        .returning("*");
      updateProject(task.todoId);
      res.status(200).json({ "message": "Updated Task Successfully" });
    } else {
      res.status(404).json({ message: "Cannot update task!!!" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "error updating task!!!" });
  }
};

const getTask = async (req, res) => {
  const id = req.params.id;
  try {
    const task = await Task.query()
      .findOne("id", id)
    console.log(task);
    if (task.userId == req.token.id) {
      res.status(200).json({ task: task });
    } else {
      res.status(404).json({ message: "task not found!!!!" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "error getting task!!!" });
  }
};

const deleteTask = async (req, res) => {
  const id = req.params.id;

  try {
    let task = await Task.query().findOne("id", id);
    if (task.userId == req.token.id) {
      task = await Task.query().findOne("id", id).del();
      res.status(200).json({ "message": "Deleted Task Successfully" });
    } else {
      res.status(404).json({ message: "cannot find task!!!" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "error deleting task!!!" });
  }
};
module.exports = {
  createTask,
  updateTask,
  getTask,
  deleteTask,
};
