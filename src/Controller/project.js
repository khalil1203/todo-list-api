const Todos = require("../db/models/project");

const createProject = async (req, res) => {
  const { title, status, description} = req.body;
  const username = req.params.username;
  try {
    if (username == req.token.username) {
    const project = await Todos.query().insert({
      title: title,
      description: description,
      todoStatus: status,
      userId:req.token.id
    });
    
      res.status(200).json({ project: project });
    } else {
      res
        .status(500)
        .json({ message: "User not allowed to create Project!!!" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: error.message });
  }
};

const getProject = async (req, res) => {
  const id = req.params.id;
  try {
    const project = await Todos.query()
      .findOne("id", id)
      .withGraphFetched("task");

    if (project.userId == req.token.id) {
      res.status(200).json({ project: project });
    } else {
      res.status(404).json({ message: "Project not found" });
    }
  } catch (error) {
    res.status(500).json({ error: "cannot get project!!!" });
  }
};

const updateProject = async (req, res) => {
  const id = req.params.id;
  const { title, description, todoStatus } = req.body;

  try {
    const project = await Todos.query().findOne({ id: id });

    if (project.userId == req.token.id) {
      project = await Todos.query()
        .findOne({ id: id })
        .update({
          title,
          description,
          todoStatus,
        })
        .returning("*");
      res.json({ "message": "Project updated successfully." });
    } else {
      res.status(404).send({ message: "Project not found" });
    }
  } catch (error) {
    res.status(500).send({ error: "cannot update project!!!" });
  }
};

const deleteProject = async (req, res) => {
  const id = req.params.id;
  try {
    const project = await Todos.query().where("id", id);
    if (project.userId == req.token.id) {
      project = await Todos.query().findOne("id", id).del().returning("*");
      res.json({"message": "Project deleted successfully."});
    } else {
      res.status(404).send({ message: "Project not found" });
    }
  } catch (error) {
    res.status(500).send({ error: "cannot delete project!!!" });
  }
};

module.exports = {
  createProject,
  getProject,
  updateProject,
  deleteProject,
};
