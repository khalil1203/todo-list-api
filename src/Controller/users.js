const config = require("../../config");
const db = require("../db/db");
const User = require("../db/models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { tokenGenerate } = require("../service/auth");

const createUser = async (req, res) => {
  const { first_name, last_name, email, password, username } = req.body;
  let pass = await bcrypt.hash(password, 10);

  try {
    const curUser = await User.query().findOne("username", username);
    if (curUser) {
      return res
        .status(400)
        .json({ message: "Username is already present!!!" });
    }
    let user = await User.query().insert({
      first_name: first_name,
      last_name: last_name,
      email: email,
      password: pass,
      username: username,
    });
    user = {
      username: username,
      first_name: first_name,
      last_name: last_name,
      email: email,
    };
    res.status(200).json({ user: user });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const getUserProfile = async (req, res) => {

  try {

    const username = req.params.username;
    let data = await User.query()
      .findOne("username", username)
      .withGraphFetched("project");

    if (data.username == req.token.username) {
      const user = {
        username: data.username,
        first_name: data.first_name,
        last_name: data.last_name,
        email: data.email,
        projects: data.project
      };
      res.status(200).json({ user: user });
    } else {
      res.status(500).json({ message: "User Not Found!!!!" });
    }
  } catch (error) {
    res.json({ message: error.message });
  }
};

const login = async (req, res) => {
  try {
    const {username} = req.body;
    let data = await User.query().findOne("username", username);
    console.log(data);
    let token = tokenGenerate(data);
    if (data.username) {
      const user = {
        username: data.username,
        first_name: data.first_name,
        last_name: data.last_name,
        email: data.email,
        projects: data.project
      };
      res.status(200).json({ user: user, token: token });
    } else {
      res.status(400).json({ message: "User Not Found,Please check user credentials!!!!" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createUser,
  getUserProfile,
  login
};
