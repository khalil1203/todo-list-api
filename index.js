const express = require("express");
const config = require("./config");
const app = express();
const port = config.PORT || 3000;
const userRoutes = require("./src/Router/userRoutes");
const projectRoutes = require("./src/Router/projectRoutes");
const taskRoutes = require("./src/Router/taskRoutes");
const setupDb = require("./src/db/db");
setupDb();
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello from home");
});

app.use("/api/user", userRoutes);
app.use("/api/project", projectRoutes);
app.use("/api/task", taskRoutes);

app.use((req, res, next) => {
  return res.status(404).json({ error: "Url not found!!!" });
});

app.listen(port, () => {
  console.log(`server is up and running on port :${port}`);
});
