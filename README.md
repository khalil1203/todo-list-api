# Todo API Documentation

This API provides endpoints to manage users, projects, and tasks for a todo list application.

## User Management

### Create User

```
https://khaliltodoapi.onrender.com/api/user
```

- Route: `/api/user`
- Method: POST
- Sample Request:
  ```json
  {
    "first_name": "Khalil123",
    "last_name": "Ullah",
    "email": "khalil123@gmail.com",
    "password": "12345"
  }
  ```
- Sample Response:
  ```json
  {
    "user": {
      "first_name": "Khalil123",
      "last_name": "Ullah",
      "email": "khalil123@gmail.com"
    }
  }
  ```
- Description: This route allows users to sign up by providing their name, email and password.

### Get User Details by Username

```
https://khaliltodoapi.onrender.com/api/user/username
```

- Route: `/api/user/:username`
- Method: GET
- Sample Response:
  ```json
  {
    "user": {
      "first_name": "Khalil123",
      "last_name": "Ullah",
      "email": "khalil123@gmail.com"
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwiaWF0IjoxNjkyNDI1OTI2fQ.PEkEN7CqKAUyFAAETi7gqCqNIpfHfySQ56gvj-W-wdM"
  }
  ```
- Description: Retrieve user details by providing the username.

## Project Management

### Create Project

```
https://khaliltodoapi.onrender.com/api/project/username
```

- Route: `/api/project/:username`
- Method: POST
- Headers: `Authorization:{jwt token}`
- Sample Request:
  ```json
  {
    "title": "Project3",
    "status": false,
    "description": "Project1 of user khalil1203"
  }
  ```
- Sample Response:
  ```json
  {
    "project": {
      "title": "Project4",
      "description": "Project4 for khalil1203",
      "todoStatus": false,
      "userId": 1,
      "id": 4
    }
  }
  ```
- Description: Create a new project with a title and description.

### Get All Projects Details

```
https://khaliltodoapi.onrender.com/api/project/3
```

- Route: `/api/project/:id`
- Method: GET
- Headers: `Authorization:{jwt token}`
- Sample Response:
  ```json
  {
    "project": {
      "id": 3,
      "title": "Project3",
      "description": "Project3 for khalil1203",
      "todoStatus": false,
      "userId": 1,
      "task": []
    }
  }
  ```
- Description: Retrieve details of all projects.

### Update Project Details

```
https://khaliltodoapi.onrender.com/api/project/project_id

```

- Route: `/api/project/:project_id`
- Method: PUT
- Headers: `Authorization:{jwt token}`
- Sample Request:
  ```json
  {
    "title": "Sample Project 1",
    "description": "Sample Project 1 description",
    "completed": true
  }
  ```
- Sample Response:
  ```json
  {
    "message": "Project updated successfully."
  }
  ```
- Description: Update the title and/or description and/or completed of a project.

### Delete Project

```
https://khaliltodoapi.onrender.com/api/project/project_id

```

- Route: `/api/project/:project_id`
- Method: DELETE
- Headers: `Authorization:{jwt token}`
- Sample Response:
  ```json
  {
    "message": "Project deleted successfully."
  }
  ```
- Description: Delete a project by providing its ID.

## Task Management

### Create Task

```
https://khaliltodoapi.onrender.com/api/task

```

- Route: `/api/task`
- Method: POST
- Headers: `Authorization: {jwt token}`
- Sample Request:
  ```json
  {
    "first_name": "Sample Task",
    "status": false,
    "todoId": 1
  }
  ```
- Sample Response:
  ```json
    {
  {
            "title": "Sample Task",
            "status": false,
            "todoId": 1,
            "userId":1,
            "id": 1
        }
    }
  ```
- Description: Create a new task within a specific project.


### get a single Task Details

```
https://khaliltodoapi.onrender.com/api/task/task_id
```

- Route: `/api/task/:task_id`
- Method: GET
- Headers: `Authorization: {jwt token}`
- Sample Response:
  ```json
  {
    "data": {
      "id": 1,
      "title": "Sample Task",
      "status": false,
      "todoId": 1,
    }
  }
  ```
- Description: Retrieve detail of a single tasks with specific task id.

### Update Task Details

```
https://khaliltodoapi.onrender.com/api/task/taskId
```

- Route: `/api/task/:task_id`
- Method: PUT
- Headers: `Authorization: {jwt token}`
- Sample Request:
  ```json
  {
    "first_name": "Updated",
    "status":true
  }
  ```
- Sample Response:
  ```json
  {
    "message": "Task updated successfuly."
  }
  ```
- Description: Update the title and/or completed of a task.

### Delete Task

```
https://khaliltodoapi.onrender.com/api/task/taskId
```

- Route: `/api/task/:task_id`
- Method: DELETE
- Headers: `Authorization: {jwt token}`
- Sample Response:
  ```json
  {
    "message": "Task deleted successfuly."
  }
  ```
- Description: Delete a task by providing its ID.
